#this program reads the text the text file of ping data and get the ping time
#from those files
import numpy as np
import matplotlib.pyplot as plot

file_name = raw_input('Please enter the file name:')
ping_file = open(file_name,"r")


#last two lines in each file have details like number of pings
#min and max number of lines.
count =0
list = []
values = []
for line in reversed(open(file_name).readlines()):
    count = count+1
    if count>2:
        break
    list.append(line)
    #print line.rstrip()


#After getting the last line of file which have data like avg,min and max
#of ping time.
    
#print "\n"+list[0]
index = list[0].find("=")
index= index+2
indexm = list[0].find("ms")

#this section takes out the values of time and spilit it.
#After splitting, values are stored in the array and can be accessed by index
#sequence of values are min/avg/max/mdev

temp = list[0][index:indexm]
values = temp.split("/")
minimum = float(values[0])
avg = values[1]
maximum = float(values[2])
print "this is the average value "+values[1]
#print "min: "+minimum+" max: "+maximum

#***************************************************#

#Loop below slice out the ping time from each row
#convert it into float and save it into array s
#also sum of all the values are calculated inside the loop and stored in total

s = []
total =0



for line in ping_file:
    #print line
    index = line.find("time=")
    if(index != -1):
           index = index+5
           #print index
           total = total+float(line[index:index+4])
           s.append(float(line[index:index+4]))
    

##for i in s:
##    print "this is the ping time"+str(i)
    
##print "Sum of all the time is "+str(total)

#Calculating arthmetic mean
am = total/len(s)
print "Airthmetic mean "+str(am)
print "Size of array"+str(len(s))

#standard deviation
sd = []
std = open("deviation.txt","w")
for i in s:
    sd.append(abs(i-am))
    std.write("\n")
    std.write(str(abs(i-am)))
std.close()
##for i in sd:
##    print "sd of all the time"+str(i)
##    
##Calculating distribution
size = int(maximum)
print "size of distributin array"+str(size)
dis = [0]*(size+2)
for i in s:
    d = i
    d = int(d)
    #temp = dis[d]
    dis[d] = dis[d]+1
    #print "distribtuion"+str(dis[d])+"for value "+str(i)
    #print "this is distribution"+str(dis[d])
target = open("data.txt",'w')    
cdis = []
index=[]
last =0
li=""
for i,d in enumerate(dis):
    if(d>1):
        print "this is difference"+str(not(abs(last-i)>2))
        index.append(i)
        cdis.append(d)
        li = str(i)+","+str(d)
        
        last = i
        target.write("\n")
        target.write(li)
        print "this is distribution"+str(d)+"for index"+str(i)

target.close()
#xlabels = []

plot.plot(index,cdis)
##i=0
##while(i<size):
##    if (i%5==0):
##        xlabels.append(i)
##    i = i+1    
###plot.axis([0,size(cdis),0,max(cdis)])
###plot.xticks(np.arange(min(cdis), max(cdis)+1, 1.0))   
plot.show()
##    
    
